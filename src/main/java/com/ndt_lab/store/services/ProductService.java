package com.ndt_lab.store.services;

import com.ndt_lab.store.dao.ProductRepository;
import com.ndt_lab.store.entity.Product;
import com.ndt_lab.store.exceptions.ProductNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service
public class ProductService {
    public static final Logger LOG = LoggerFactory.getLogger(ProductService.class);

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product getProductById(Long id) {
        Product product = productRepository.findProductById(id)
                .orElseThrow(() -> new ProductNotFoundException("Product with id=" + id + " not found"));
        return product;
    }

    public Product createProduct(Product product) {
        Product newProduct = new Product();
        newProduct.setArticle(product.getArticle());

        LOG.info("Saving product with article: {}", product.getArticle());
        return productRepository.save(newProduct);
    }

    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }
}
