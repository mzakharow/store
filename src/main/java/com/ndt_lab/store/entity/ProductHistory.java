package com.ndt_lab.store.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sto_product_histories")
//@TypeDef(name = "json", typeClass = JsonType.class)
public class ProductHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long product_history_id;
    @ManyToOne(fetch = FetchType.EAGER)
    private Product productId;
    @Column(nullable = false)
    private String name;
    private String ndt_method;
    private BigDecimal price;
    private BigDecimal discount;
    @Column(nullable = false)
    private Boolean marked_to_order;
    @ManyToOne(fetch = FetchType.EAGER)
    private Manufacturer manufacturer_id;
    @ManyToOne(fetch = FetchType.EAGER)
    private Provider provider_id;
    @Column(nullable = false)
    private String description;

//    @Type(type = "json")
//    @Column(columnDefinition = "jsonb", nullable = false)
//    private Map specifications = new HashMap<>();

    @Column(nullable = false)
    private LocalDateTime begin_date;
    private LocalDateTime end_date;
    @Column(nullable = false)
    private Long navi_create_user;
    @Column(nullable = false)
    private LocalDateTime navi_create_date;
    private int navi_update_user;
    private LocalDateTime navi_update_date;

    @PrePersist
    protected void noCreate() {
        this.navi_update_date = LocalDateTime.now();
    }
}

