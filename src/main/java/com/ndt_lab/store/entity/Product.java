package com.ndt_lab.store.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sto_products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "product_id", unique = true)
    private Long id;
    @Column(unique = true, updatable = false)
    private String article;
//    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "productId")
    private List<ProductHistory> historyList = new ArrayList<>();

    public void addHistory(ProductHistory history) {
        historyList.add(history);
        history.setProductId(this);
    }

}
