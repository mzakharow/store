package com.ndt_lab.store.dao;

import com.ndt_lab.store.entity.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface ProviderRepository extends JpaRepository<Provider, Long> {
    Optional<Provider> findById(Long id);
}
