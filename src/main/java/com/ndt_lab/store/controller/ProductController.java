package com.ndt_lab.store.controller;

import com.ndt_lab.store.dao.ProductRepository;
import com.ndt_lab.store.entity.Product;
import com.ndt_lab.store.exceptions.ProductNotFoundException;
import com.ndt_lab.store.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api")
@CrossOrigin
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    public List<Product> getAllProducts() {
        List<Product> allProducts = productRepository.findAll();
        return allProducts;
    }

//    @GetMapping("/product/{id}")
//    public Product getProduct(@PathVariable Long id) {
//        Optional<Product> product = productRepository.findProductById(id);
//        if (product.isEmpty()) {
//            throw new ProductNotFoundException("Product with ID = " + id + " not found");
//        }
//        return product.get();
//    }

    @GetMapping("/product/{id}")
    public ResponseEntity<Product> getProduct(@PathVariable Long id) {
        return new ResponseEntity<>(productService.getProductById(id), HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/product")
    public ResponseEntity<Object> addNewProduct(@Valid @RequestBody  Product product) {
        Product newProduct = productService.createProduct(product);
        return new ResponseEntity<>(newProduct, HttpStatus.OK);
    }

//    @PostMapping("/product/{id}/delete")
    @DeleteMapping({"/product/{id}"})
    public ResponseEntity<Product> deleteProduct(@PathVariable("id") Long id) {
        productService.deleteProduct(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
